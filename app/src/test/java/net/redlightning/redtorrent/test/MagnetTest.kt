/**
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http:></http:>//www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test

import java.net.URLDecoder
import net.redlightning.redtorrent.common.TorrentService
import android.content.Intent
import android.net.Uri
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test
import java.io.UnsupportedEncodingException

/**
 * UI tests for RedTorrent and TorrentActivity
 *
 * @author Michael Isaacson
 * @version 19.6.12
 */
class MagnetTest {
    @Test
    fun testHandleMagnet() {
        val intent = Intent()
        //Sintel
        // magnet:?xt=urn:btih:08ada5a7a6183aae1e09d831df6748d566095a10&dn=Sintel&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel.torrent
        try {
            //Ubuntu
            val decoded = URLDecoder.decode("magnet:?xt=urn:btih:bbb6db69965af769f664b6636e7914f8735141b3&dn=Ubuntu-12.04-desktop-i386.iso&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80&tr=udp%3A%2F%2Ftracker.istole.it%3A6969&tr=udp%3A%2F%2Ftracker.ccc.de%3A80", "UTF-8")
            intent.data = Uri.parse(decoded)
            assertTrue(TorrentService.handleMagnet(intent))
        } catch (o_O : UnsupportedEncodingException) {
            fail("Unsupported encoding exception for magnet URI")
        }
    }

    @After
    fun cleanup() {
        //TODO Cleanup and remove the downloaders
    }
}
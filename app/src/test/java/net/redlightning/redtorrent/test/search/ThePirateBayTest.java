/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.test.search;

import android.util.Log;

import net.redlightning.redtorrent.search.ThePirateBay;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertNotNull;

/**
 * Tests searching for torrents on ThePirateBay.org
 * 
 * @author Michael Isaacson
 * @version 11.12.20
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class ThePirateBayTest {
	/**
	 * Test method for {@link net.redlightning.redtorrent.search.ThePirateBay#search()}.
	 */
	@Test
	public void testSearch() {
		PowerMockito.mockStatic(Log.class);
		final ThePirateBay bay = new ThePirateBay("Ubuntu", null);
		assertNotNull(bay.search());
	}
}
/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.content_main.*

import android.os.Environment
import easyfilepickerdialog.kingfisher.com.library.model.SupportFile
import android.util.Log
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.Spinner

import easyfilepickerdialog.kingfisher.com.library.model.DialogConfig
import easyfilepickerdialog.kingfisher.com.library.view.FilePickerDialogFragment
import net.redlightning.redtorrent.common.*
import net.redlightning.redtorrent.search.TorrentSearch
import java.io.File


class MainActivity : AppCompatActivity() {
    var handler: MessageHandler? = null
    val FILE_BROWSER = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        //val preferences = PreferenceManager.getDefaultSharedPreferences(this)


        handler = MessageHandler(this)
        //Start the service
        startService(Intent(baseContext, RedService::class.java))
        TorrentService.setUiHandler(handler)

        torrent_recycler_view.layoutManager =  LinearLayoutManager(this)
        torrent_recycler_view.adapter = TorrentCardListAdapter(ArrayList<String>(), this)
    }

    override fun onCreateOptionsMenu(menu: Menu) : Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.miAdd -> {
            val dialogConfig = DialogConfig.Builder()
                    .enableMultipleSelect(false) // Enable the ability to select multiple
                    .enableFolderSelect(false) // TODO Enable the ability to select the entire folder
                    .initialDirectory(Environment.getExternalStorageDirectory().absolutePath)
                    .supportFiles(SupportFile(".torrent", R.drawable.ic_torrent)) // default is showing all file types.
                    .build()

            FilePickerDialogFragment.Builder()
                    .configs(dialogConfig)
                    .onFilesSelected(object : FilePickerDialogFragment.OnFilesSelectedListener {
                        override fun onFileSelected(list: List<File>) {
                            for (file in list) {
                                //TODO Add torrents
                                Log.e("FilePicker", "Selected file: " + file.absolutePath)
                                TorrentService.addTorrent(applicationContext, file.absolutePath, null, null)
                            }
                        }
                    })
                    .build()
                    .show(supportFragmentManager, null)
            true
        }
        R.id.miSearch -> {
            onSearchRequested()
            true
        }
        R.id.miStop -> {
            true
        }
        R.id.miSettings -> {
            startActivity(Intent(this, SettingsActivity2::class.java))
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()

        if (intent.data != null) {
            if (intent.data!!.toString().startsWith("magnet:")) {
                TorrentService.handleMagnet(intent)
            } else {
                //pickerDialog = TorrentFilePickerDialog(this, intent.data!!.toString().replace("file://", "")).build()
                //pickerDialog.show()
                intent.data = null
            }
        }
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
//        super.onActivityResult(requestCode, resultCode, data)
//        var pickerDialog: AlertDialog? = null
//        if (FILE_BROWSER == requestCode && resultCode == Activity.RESULT_OK) {
//            pickerDialog = TorrentFilePickerDialog(this, data.getStringExtra("file")).build()
//            pickerDialog.show()
////            pickerDialog.getButton(Dialog.BUTTON_NEUTRAL).setOnClickListener((view) -> {
////                if ("Clear All" == (view as Button).text) {
////                    (view as Button).text = getString(R.string.select_all)
////                    TorrentFilePickerListener.selectAll(pickerDialog, false)
////                } else if ("Select All" == (view as Button).text) {
////                    (view as Button).text = getString(R.string.clear_all)
////                    TorrentFilePickerListener.selectAll(pickerDialog, true)
////                }
////            })
//        }
//    }

    /*
	 * (non-Javadoc)
	 * @see android.app.Activity#onSearchRequested()
	 */
    override fun onSearchRequested(): Boolean {
        val builder = AlertDialog.Builder(this)
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout = inflater.inflate(R.layout.search, findViewById(R.id.searchDialog))

            val providers = layout.findViewById<Spinner>(R.id.searchProvider)
            val adapter = ArrayAdapter.createFromResource(this, R.array.search_providers, android.R.layout.simple_spinner_item)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            providers.adapter = adapter
            providers.setSelection(Settings.getSearchProvider())

            val searchListener = TorrentSearch(this, R.layout.search_results, layout.findViewById(R.id.searchInput), providers)

            builder.setTitle(R.string.search)
            builder.setIcon(android.R.drawable.ic_menu_search)
            builder.setView(layout)
            builder.setPositiveButton(R.string.search, searchListener)
            builder.setNegativeButton(R.string.cancel, null)
            builder.create().show()
        return false
    }
}

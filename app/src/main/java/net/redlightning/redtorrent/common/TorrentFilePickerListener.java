/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.util.ArrayList;
import java.util.List;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;

/**
 * Handle when the user clicks on the dialog that allows them to choose a
 * setting type
 * 
 * @author Michael Isaacson
 * @version 17.9.8
 */
public class TorrentFilePickerListener implements OnMultiChoiceClickListener, OnClickListener {
	//private static final String TAG = TorrentFilePickerListener.class.getSimpleName();
	private transient static CharSequence[] files;
	private transient static boolean[] selected;
	private transient Context context;
	private transient String torrentFile;

	/**
	 * @param context the calling Activity
	 * @param itemList the list of files in the Torrent
	 * @param selectedList a boolean array to keep track of which files are
	 *            selected
	 * @param torrentFile the Torrent file object
	 */
	protected TorrentFilePickerListener(final Context context, CharSequence[] itemList, boolean[] selectedList, String torrentFile) {
		this.context = context;
		files = itemList;
		selected = selectedList;
		this.torrentFile = torrentFile;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * android.content.DialogInterface.OnMultiChoiceClickListener#onClick(android
	 * .content.DialogInterface, int, boolean)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		if (which != Dialog.BUTTON_NEGATIVE && which != Dialog.BUTTON_NEUTRAL && which != Dialog.BUTTON_POSITIVE && which < selected.length && which > -1) {
			selected[which] = isChecked;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * android.content.DialogInterface.OnClickListener#onClick(android.content
	 * .DialogInterface, int)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which) {
        final List<String> pickedFiles = new ArrayList<>();
		dialog.dismiss();

		if (which == Dialog.BUTTON_POSITIVE) {
			for (int i = 0; i < files.length; i++) {
				if (selected[i]) {
					pickedFiles.add(files[i].toString());
				}
			}
			if (pickedFiles.size() > 0) {
				TorrentService.addTorrent(context, torrentFile, pickedFiles, null);
			}
		}
	}

	/**
	 * Clears all the selected files
	 */
	static void selectAll(AlertDialog dialog, boolean select) {
		for (int i = 0; i < TorrentFilePickerListener.selected.length; i++) {
			dialog.getListView().setItemChecked(i, select);
			selected[i] = select;
		}
	}
}

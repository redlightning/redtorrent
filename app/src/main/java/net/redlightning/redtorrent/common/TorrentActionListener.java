/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

/**
 * @author Michael Isaacson
 * @version 10.12.23
 */
public class TorrentActionListener implements OnClickListener {
	//private static final String TAG = TorrentActionListener.class.getSimpleName();
	private final transient int position;
	private final transient Context context;

	/**
	 * Constructor
	 * 
	 * @param activity the calling activity
	 * @param pos the position of the item in the ListView
	 */
	protected TorrentActionListener(final Context activity, final int pos) {
		position = pos;
		context = activity;
	}

	/*
	 * (non-Javadoc)
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(final DialogInterface dialog, final int which) {
	    //TODO Update to new handler
//		final Downloader item = TorrentActivity.getAdapter().getItem(position);
//		if (which == 0) {
//			//STOP/RESUME
//			if(item != null && item.getManager() != null) {
//				if (item.getManager().getTorrent().getStatus().equals("Stopped") || item.getManager().getTorrent().getStatus().equals("Queued")) {
//					TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.START, item));
//				} else {
//					item.stopDownloading();
//					item.getManager().getTorrent().setStatus("Stopped");
//				}
//			}
//		} else {
//			//REMOVE
//			TorrentService.removeTorrent(context, position);
//		}
//		TorrentActivity.getAdapter().notifyDataSetChanged();
		dialog.dismiss();
	}
}

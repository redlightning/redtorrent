/*
 * Copyright Michael Isaacson This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.google.inject.Module;

import androidx.preference.PreferenceManager;
import bt.Bt;
import bt.data.Storage;
import bt.data.file.FileSystemStorage;
import bt.dht.DHTConfig;
import bt.dht.DHTModule;
import bt.net.InetPeerAddress;
import bt.runtime.BtClient;
import bt.runtime.Config;

/**
 * @author Michael Isaacson
 * @version 11.3.8
 */
public class Downloader {
	private static final String TAG = Downloader.class.getSimpleName();
	private String name;
	private transient float rate;
	private transient float percentDone;
	private int peers;
	private boolean complete = false;
    private Config config;
    private Module dhtModule;
    private BtClient client;

	/**
	 * Constructor
	 *
     * @param context the current application context
	 * @param file the path to the .torrent file
	 */
	public Downloader(final Context context, final File file) {
	    name = file.getName();

        //TODO Add to downloading files list to track for restarts, etc
//        configure(context);
//        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
//        Storage storage = new FileSystemStorage(Paths.get(path));
//        Options options;
//        options.getMetainfoFile() != null) {
//            clientBuilder = clientBuilder.torrent(toUrl(options.getMetainfoFile()));
//        Torrent torrent = TorrentFile
//
//        Supplier<Torrent> supplier; // = new Supplier<>();
//
//        client = Bt.client()
//                .config(config)
//                .storage(storage)
//                .torrent(supplier)
//                .autoLoadModules()
//                .module(dhtModule)
//                .stopWhenDownloaded()
//                .build();
//        Log.d(TAG, "Start download of " + name);
//        //long t0 = System.currentTimeMillis();
//        client.startAsync(state -> {
//                    peers = state.getConnectedPeers().size();
//                    percentDone = (((float)state.getPiecesComplete()) / state.getPiecesTotal()) * 100;
//                }
//                , 1000).join();
        //System.err.println("Done in " + Duration.ofMillis(System.currentTimeMillis() - t0));
	}

	
	/**
	 * Constructor
	 *
     * @param context the current app context
	 * @param magnet the magnet URI
	 */
	public Downloader(final Context context, final String magnet) {
	    try {
            Map<String, List<String>> uriParts = splitQuery(new URL(magnet));
            List<String> downloadNames = uriParts.get("dn");
            if(downloadNames != null) {
                name = downloadNames.get(0);
            }
        } catch (MalformedURLException | UnsupportedEncodingException o_O)  {
	        Log.e(TAG, "Invalid magnet URL");
        }

	    //TODO Add magnet to downloading files list to track for restarts, etc
	    configure(context);
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        Storage storage = new FileSystemStorage(Paths.get(path));
        client = Bt.client()
                .config(config)
                .storage(storage)
                .magnet(magnet)
                .autoLoadModules()
                .module(dhtModule)
                .stopWhenDownloaded()
                .build();
        Log.d(TAG, "Start download of " + magnet);
        //long t0 = System.currentTimeMillis();
        client.startAsync(state -> {
                    peers = state.getConnectedPeers().size();
                    percentDone = (((float)state.getPiecesComplete()) / state.getPiecesTotal()) * 100;
                }
                        , 1000).join();
        //System.err.println("Done in " + Duration.ofMillis(System.currentTimeMillis() - t0));
	}

	private void configure(Context context) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final int port = prefs.getInt("port", 6891);

	    config = new Config() {
            @Override
            public int getNumOfHashingThreads() {
                return Runtime.getRuntime().availableProcessors() * 2;
            }

            @Override
            public int getAcceptorPort() {
                return port;
            }
        };

	    dhtModule = new DHTModule(new DHTConfig() {
            @Override
            public Collection<InetPeerAddress> getBootstrapNodes() {
                return Collections.singleton(new InetPeerAddress(config.getAcceptorAddress().getHostAddress(), port));
            }
        });

    }

    public int getPeers(){ return peers; }

	float getPercentDone() {
		return percentDone;
	}

	float getRate() {
		return rate;
	}

	String getName() {
	    return name;
    }

	/**
	 * Stop the tracker updates and close any open files
	 */
	void stopDownloading() {
	    client.stop();
	}

    /**
     * Parse a URL and get the query parts in a map
     *
     * @param url the URL with GET query parameters to parse
     * @return a map of query components
     * @throws UnsupportedEncodingException when the URL is invalidly encoded
     */
    private static Map<String, List<String>> splitQuery(URL url) throws UnsupportedEncodingException {
        final Map<String, List<String>> query_pairs = new LinkedHashMap<>();
        final String[] pairs = url.getQuery().split("&");
        for (String pair : pairs) {
            final int idx = pair.indexOf("=");
            final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
            if (!query_pairs.containsKey(key)) {
                query_pairs.put(key, new LinkedList<>());
            }
            final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
            query_pairs.get(key).add(value);
        }
        return query_pairs;
    }

    private static URL toUrl(File file) {
        try {
            return file.toURI().toURL();
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Unexpected error", e);
        }
    }
}
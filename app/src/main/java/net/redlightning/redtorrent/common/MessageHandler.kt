/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.common

import net.redlightning.redtorrent.MainActivity
import net.redlightning.redtorrent.search.TorrentFileDownloader
import net.redlightning.redtorrent.search.TorrentSearch
import android.os.Handler
import android.os.Message
import android.widget.Toast

/**
 * Handle messages from the service to the UI to display errors and update free memory and sdcard space
 *
 * @author Michael Isaacson
 * @version 19.6.6
 */
class MessageHandler

(private val context: MainActivity) : Handler() {

    @Synchronized
    override fun handleMessage(msg: Message) {
        //TODO Update to new MainActivity
//        if (TorrentActivity.getAdapter() != null) {
//            TorrentActivity.getAdapter().notifyDataSetChanged()
//        }
//
//        //Show an error message, if we received one
//        if (msg.what == TorrentSearch.ERROR || msg.what == TorrentFileDownloader.DOWNLOAD_ERROR) {
//            Toast.makeText(context, msg.obj.toString(), Toast.LENGTH_LONG).show()
//        }
    }
}
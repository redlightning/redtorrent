/**
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.search;

import java.util.ArrayList;
import java.util.List;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentFilePickerDialog;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.BadTokenException;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Handles when the user taps the search button
 * 
 * @author Michael Isaacson
 * @version 11.12.21
 */
public class TorrentSearch implements OnClickListener {
	private static final String TAG = TorrentSearch.class.getCanonicalName();
	public static final String ERROR_MESSAGE = "Search failed or 0 results";
	public static final int ERROR = 10000;
	public static final int SUCCESS = 10001;
	public static Handler handler;
	public static ProgressDialog progressDialog;
	public static AlertDialog resultsDialog;
	
	private final Context context;
	private final EditText input;
	private final Spinner searchProviders;
	private final int resultListLayout;

	/**
	 * Constructor
	 * 
	 * @param context the current application context
	 * @param input the EditText box containing the string to search for
	 */
	public TorrentSearch(final Context context, final int resultListLayout, final EditText input, final Spinner searchProviders) {
		this.context = context;
		this.input = input;
		this.resultListLayout = resultListLayout;
		this.searchProviders = searchProviders;
	}

	/**
	 * @return the handler
	 */
	public static synchronized Handler getHandler() {
		return handler;
	}

	/**
	 * @return the progressDialog
	 */
	public static synchronized ProgressDialog getProgressDialog() {
		return progressDialog;
	}

	/**
	 * @return the resultsDialog
	 */
	public static synchronized AlertDialog getResultsDialog() {
		return resultsDialog;
	}

	/**
	 * 
	 * @param theHandler
	 */
	private static synchronized void setHandler(final Handler theHandler) {
		handler = theHandler;
	}

	/**
	 * @param progressDialog the progressDialog to set
	 */
	public static synchronized void setProgressDialog(final ProgressDialog progressDialog) {
		TorrentSearch.progressDialog = progressDialog;
	}

	/**
	 * @param resultsDialog the resultsDialog to set
	 */
	public static synchronized void setResultsDialog(final AlertDialog resultsDialog) {
		TorrentSearch.resultsDialog = resultsDialog;
	}

	/**
	 * 
	 * @param searchResults
	 */
	public void buildSearchResultList(final JSONArray searchResults) {
		final List<JSONObject> torrentList = new ArrayList<JSONObject>(20);
		for (int i = 0; i < searchResults.length(); i++) {
			try {
				torrentList.add(searchResults.getJSONObject(i));
			} catch (final JSONException e) {
				e.printStackTrace();
			}
		}

		final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View view = inflater.inflate(resultListLayout, null);
		final ListView resultListView = view.findViewById(SearchResultListAdapter.searchResultsList);

		// Generate the ListView
		resultListView.setAdapter(new SearchResultListAdapter(context, android.R.layout.simple_list_item_1, torrentList));
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Search Results");
		builder.setView(view);
		builder.setPositiveButton("Cancel", null);
		setResultsDialog(builder.create());
		getResultsDialog().show();
	}

	/**
	 * 
	 * @param message
	 */
	public void buildErrorDialog(final String message) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Search Failed");
		//builder.setIcon(R.drawable.ic_dialog_info);
		builder.setMessage(message);
		builder.setPositiveButton("Ok", null);
		setResultsDialog(builder.create());
		try {
			getResultsDialog().show();
		} catch (final BadTokenException e) {
			Log.e(TAG, "Unable to create Search Error Dialog: " + e.getMessage());
		}
	}

	/**
	 * @return the context
	 */
	public synchronized Context getContext() {
		return context;
	}

	/**
	 * @return the input
	 */
	public synchronized EditText getInput() {
		return input;
	}

	/**
	 * @return the resultListLayout
	 */
	public synchronized int getResultListLayout() {
		return resultListLayout;
	}

	/*
	 * (non-Javadoc)
	 * @see android.content.DialogInterface.OnClickListener#onClick(android.content.DialogInterface, int)
	 */
	@Override
	public void onClick(final DialogInterface dialog, final int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			if (input.getText().toString().trim().length() > 0) {

				//Create the main handler on the main thread so it is bound to the main thread's message queue.
				setHandler(new Handler() {
					@Override
					public synchronized void handleMessage(final Message msg) {
						try {
							progressDialog.dismiss();
						} catch (final IllegalArgumentException e) {
							Log.e(TAG, "Unable to dismiss dialog: " + e.getMessage());
						}

						if (msg.what == SUCCESS) {
							final JSONObject json = (JSONObject) msg.obj;

							try {
								final JSONArray array = json.getJSONObject("items").getJSONArray("list");
								buildSearchResultList(array);
							} catch (final JSONException e) {
								buildErrorDialog("0 results returned");
								Log.e(TAG, "JSON error getting array");
							}
						} else if (msg.what == TorrentFileDownloader.DOWNLOAD_SUCCESS) {
							new TorrentFilePickerDialog(context, msg.obj.toString()).build().show();
						} else if (msg.what == ERROR) {
							buildErrorDialog("Search failed or 0 results");
							//TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(ERROR, "Search failed, please try again"));
						} else if (msg.what == TorrentFileDownloader.DOWNLOAD_ERROR) {
							buildErrorDialog("Download of .torrent failed, please try again");
							//TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentFileDownloader.DOWNLOAD_ERROR, "Download of .torrent failed, please try again"));
						}
					}
				});

				setProgressDialog(ProgressDialog.show(context, "", "Searching...", true));
				getProgressDialog().setCanceledOnTouchOutside(true);

				Settings.setSearchProvider(searchProviders.getSelectedItemPosition());
				if (searchProviders.getSelectedItemPosition() == 0) {
					final ISOHunt provider = new ISOHunt(input.getText().toString().trim(), handler);
					provider.start();
				} else {
					
					final ThePirateBay provider = new ThePirateBay(input.getText().toString().trim(), handler);
					provider.start();
				}
			} else {
				final AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Invalid Search");
				builder.setMessage("Search text cannot be blank");
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.create().show();
			}
		}
	}
}
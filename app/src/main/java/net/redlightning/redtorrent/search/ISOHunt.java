/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent.search;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import android.os.Handler;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * From the ISO Hunt documentation
 * Calls to the isoHunt JSON api have URL format like this:
 * 
 * http://isohunt.com/js/json.php?ihq=ubuntu&start=21&rows=20&sort=seeds
 * 
 * Parameters:
 * ihq = Takes url encoded value as requested search query.
 * start = Optional. Starting row number in paging through results set. First page have start=1, not 0. Defaults to 1.
 * rows = Optional. Results to return, starting from parameter "start". Defaults to 100.
 * sort = Optional. Defaults to composite ranking (over all factors such as age, query relevance, seed/leechers counts and votes). Parameter takes
 * only values of "seeds", "age" or "size", where seeds sorting is combination of seeds+leechers. Sort order defaults to descending.
 * order = Optional, can be either "asc" or "desc". Defaults to descending, in conjunction with sort parameter.
 * noSL = Deprecated. All torrents are returned, whether they have seeds/leechers or not.
 * 
 * Above same JSON call would search for "ubuntu", returning 2nd page of results, with 20 results per page. Sorts by seeds and leechers, and include
 * torrents with S/L stats only. Rows have upper limit of 100, and start+rows have maximum possible limit of 1000.
 * 
 * NOTE:
 * In using our search API, you are free to do with it as you wish on condition that if your app is available publicly to users, you must link to
 * torrent details pages on isoHunt.com, whether you link to the .torrent files or not. We reserve the right to ban you from using our API if you
 * don't follow this simple rule. Refer to Louish's iPhone app for a good example of including links to our torrent details pages. Our torrent details
 * pages have URLs like this: http://isohunt.com/torrent_details/28289948/ubuntu?tab=summary
 * 
 * While we don't require developer tokens or place hard limits on api calls usage, excessive calls will also result in bans. If you think your app
 * will consistently sustain multiple calls per second to our api, email admin at this site's domain first.
 * You are free to promote your app using our API, by replying under this post or post under this forum. If your app is really good, we'll likely want
 * to spotlight it on isoHunt's frontpage. Multiple posts to promote your app on our forum or comments is not allowed however, and will be treated as
 * spam.
 * 
 * @author Michael Isaacson
 * @version 11.12.21
 */
public class ISOHunt extends Thread {
	private static final String TAG = ISOHunt.class.getCanonicalName();
	private transient String query;
	private transient Handler handler;

	/**
	 * Constructor
	 * 
	 * @param query the item to search for
	 * @param handler the handler to send messages to
	 */
	public ISOHunt(final String query, final Handler handler) {
		super("ISOHunt");
		this.query = query;
		this.handler = handler;
	}

	/*
	 * (non-Javadoc)
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	public void run() {
		final JSONObject json = search();
		if (json == null && handler != null) {
			handler.sendMessage(handler.obtainMessage(TorrentSearch.ERROR, TorrentSearch.ERROR_MESSAGE));
		} else if (handler != null) {
			handler.sendMessage(handler.obtainMessage(TorrentSearch.SUCCESS, json));
		}
	}

	/**
	 * Submit the search query to isoHunt, and retrieve the results
	 * 
	 * @return the results of the search, null if there was an error
	 */
	public JSONObject search() {
		JSONObject returnVal = null;
		try {
			final URL source = new URL("http://isohunt.com/js/json.php?rows=20&sort=seeds&ihq=" + URLEncoder.encode(query, "UTF-8"));
			final URLConnection uc = source.openConnection();
			final InputStream inputStream = uc.getInputStream();
			final JSONObject json = new JSONObject(new JSONTokener(convert(inputStream, Charset.forName("UTF-8"))));
			inputStream.close();
			returnVal = json;
		} catch (final MalformedURLException e) {
			Log.i(TAG, "MalformedURLException: " + e.getMessage());
		} catch (final IOException e) {
			Log.i(TAG, "IOException: " + e.getMessage());
		} catch (final JSONException e) {
			Log.i(TAG, "JSONException: " + e.getMessage());
		}
		return returnVal;
	}

	/**
	 * @return the current query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * @return the Handler object
	 */
	public Handler getHandler() {
		return handler;
	}

    public String convert(InputStream inputStream, Charset charset) throws IOException {

        StringBuilder stringBuilder = new StringBuilder();
        String line;

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset))) {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }

        return stringBuilder.toString();
    }
}
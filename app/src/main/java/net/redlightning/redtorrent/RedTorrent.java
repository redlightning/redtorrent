/*
 * Copyright Michael Isaacson. This file is part of Red Torrent. Red Torrent is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any
 * later version. Red Torrent is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the
 * GNU General Public License along with Red Torrent. If not, see <http://www.gnu.org/licenses/>.
 */
package net.redlightning.redtorrent;

import net.redlightning.redtorrent.common.DownloadListAdapter;
import net.redlightning.redtorrent.common.Settings;
import net.redlightning.redtorrent.common.TorrentService;
import net.redlightning.redtorrent.search.SearchResultListAdapter;
import net.redlightning.redtorrent.search.TorrentSearch;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

/**
 * Main activity for Red Torrent
 * 
 * @author Michael Isaacson
 * @version 17.8.29
 */
public class RedTorrent extends Activity {
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(final Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.main);

		final ImageView searchImage = findViewById(R.id.searchImage);
		searchImage.setOnClickListener((view) -> onSearchRequested());

		final ImageView menuImage = findViewById(R.id.menuImage);
		final Activity context = this;
		menuImage.setOnClickListener((view) -> context.openOptionsMenu());
		
//		handler = new MessageHandler(RedTorrent.this);
//		//Start the service
//		startService(new Intent(getBaseContext(), RedService.class));
//		TorrentService.setUiHandler(handler);
		// downloaders = TorrentService.getDownloaderList();

		// Generate the ListView
		DownloadListAdapter.setViews(R.layout.list_item, R.id.text, R.id.speed, R.id.status, R.id.allocation, R.id.peers, R.id.error_message, R.id.progress);
		// setAdapter(new DownloadListAdapter(this, R.layout.list_item, downloaders));
		// setListAdapter(getAdapter());

		//Set up search controls
		SearchResultListAdapter.initialize(R.layout.search_result_item, R.id.search_results_list, R.id.torrentName, R.id.size, R.id.peers);
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onMenuItemSelected(int, android.view.MenuItem)
	 */
	@Override
	public boolean onMenuItemSelected(final int itemNum, final MenuItem item) {
//		switch (item.getItemId()) {
//			case MENU_EXIT:
//				TorrentService.getHandler().sendMessage(TorrentService.getHandler().obtainMessage(TorrentService.QUIT));
//				stopService(new Intent(getBaseContext(), RedService.class));
//				onDestroy();
//				finish();
//				break;
//			case MENU_SEARCH:
//				onSearchRequested();
//				break;
//			case MENU_STOP_ALL:
//				final Set<String> keys = TorrentService.getDownloaders().keySet();
//				for (final String key : keys) {
//					Downloader dl = TorrentService.getDownloaders().get(key);
//					if(dl != null) dl.stopDownloading();
//				}
//				getAdapter().notifyDataSetChanged();
//				Toast.makeText(this, R.string.allStopped, Toast.LENGTH_SHORT).show();
//				break;
//			default:
//				break;
//		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();

		if (getIntent().getData() != null) {
			if (getIntent().getData().toString().startsWith("magnet:")) {
				TorrentService.handleMagnet(getIntent());
			} else {
				// pickerDialog = new TorrentFilePickerDialog(this, getIntent().getData().toString().replace("file://", "")).build();
				// pickerDialog.show();
				getIntent().setData(null);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onSearchRequested()
	 */
	@Override
	public boolean onSearchRequested() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		if(inflater != null) {
            final View layout = inflater.inflate(R.layout.search, findViewById(R.id.searchDialog));

            final Spinner providers = layout.findViewById(R.id.searchProvider);
            final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.search_providers, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            providers.setAdapter(adapter);
            providers.setSelection(Settings.getSearchProvider());

            final TorrentSearch searchListener = new TorrentSearch(this, R.layout.search_results, layout.findViewById(R.id.searchInput), providers);

            builder.setTitle(R.string.search);
            builder.setIcon(android.R.drawable.ic_menu_search);
            builder.setView(layout);
            builder.setPositiveButton(R.string.search, searchListener);
            builder.setNegativeButton(R.string.cancel, null);
            builder.create().show();
        }
		return false;
	}
}